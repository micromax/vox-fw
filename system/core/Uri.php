<?php

class uri {
    //this is array
    //$_SERVER['PATH_INFO'];
    
    private $URI;
    public function __construct() 
    {
        if(isset($_SERVER['PATH_INFO']))
        {
            $uri = strip_tags(trim($_SERVER['PATH_INFO'])); 
            $uri = explode("/", $uri);
            $this->URI = $uri;
        }
    }
    
    
    public function sig($n){
        $n = intval($n);
        if(array_key_exists($n, $this->URI)){
            return $this->URI[$n];
        }
        else {
            return FALSE;
        }
        
    }
    
    
    
    
}

