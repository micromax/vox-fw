<?php

class input {
    
    
    private $secutry = true;
    
    private $base64 = false;
    
    




    public function __construct($config = null) {
        if($config != null && is_array($config))
         {
            $this->secutry = $config["stripHtml"];
            $this->base64 = $config["base64"];
            
            
         }
         
         
    }
    
    
    public function post($post)
    {
        if(isset($_POST[$post]))
        {
            $result = $_POST[$post];
            if($this->secutry == true)
            {
                $result = htmlspecialchars($result);
            }
            
            if($this->base64 == true)
            {
                $result = base64_encode($result);
            }
            
            return $result ;
            
        }  else {
            return FALSE;    
        }
        
    }
    
    
    public function get($get)
    {
        if(isset($_GET[$get]))
        {
            $result = $_GET[$get];
            if($this->secutry == true)
            {
                $result = htmlspecialchars($result);
            }
            
            if($this->base64 == true)
            {
                $result = base64_encode($result);
            }
            
            return $result ;
            
        }  else {
            return FALSE;    
        }
        
    }
    
    
    public function cookie($cookie)
    {
        if(isset($_COOKIE[$cookie]))
        {
            $result = $_COOKIE[$cookie];
            if($this->secutry == true)
            {
                //strip_tags
                $result = htmlspecialchars($result);
            }
            
            if($this->base64 == true)
            {
                $result = base64_encode($result);
            }
            
            return $result ;
            
        }  else {
            return FALSE;    
        }
        
    }
    
    
    public function session($session)
    {
       
        if(isset($_SESSION[$session]))
        {
            $result = $_SESSION[$session];
            if($this->secutry == true)
            {
                //strip_tags
                $result = htmlspecialchars($result);
            }
            
            if($this->base64 == true)
            {
                $result = base64_encode($result);
            }
            
            return $result ;
            
        }  else {
            return FALSE;    
        }
    }
    
    
    
    
    
    
}

