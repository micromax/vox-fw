<?php
include_once 'Controller'.EXT;
class Model extends AB_root{

    public $cm;
    
    public function __construct() {
        parent::__construct();
        $this->_up_lib();
        
        $this->cm = ucfirst(get_class($this));
       
    }
    
    
    public function _up_lib(){
        $AB = AB_instance();
        
        $dec_ob = array_keys(get_object_vars($AB));
        
        foreach ($dec_ob as $key)
        {
            if(!isset($this->$key) AND $key != $this->cm  AND $key != 'db')
            {
                $this->$key = $AB->$key;
            }

        }

    }
    
}

