<?php

require_once "active_record".EXT ;
class db extends active_record
{
  
    
    private $THEQUIRY = '';
    
  
    
    private $prepaerd = '';

   
    private static $Where = '';
    
    
    private static $Select = null;
    
    private static $joins = '';
    
    private static $placeholders = '';
    
    private static $order = '';


    public function __construct($dsn, $username = null, $passwd = null, $options = null) {
        parent::__construct($dsn, $username, $passwd, $options);
        
    }
    
    
     
    private static function  fetchMODE($mode = '')
    {
        switch ($mode)
        {
            case "assoc":
                return PDO::FETCH_ASSOC;
            break;
                
            
            case "obj":
                return PDO::FETCH_OBJ;
            break;
            
            case 'bound':
                return PDO::FETCH_BOUND;
            break;
                
            case 'col':
                return PDO::FETCH_COLUMN;
            break;
            
            case 'class':
                return PDO::FETCH_CLASS;
                break;
            case 'num' :
                return PDO::FETCH_NUM;
            break;
        
            default :
                return PDO::FETCH_BOTH;
             break;
            
        }
        
        
        
    }
    
    
    
    
    public function query($sql) 
    {
        
        (string) $STRING = $sql;
       
        $this->THEQUIRY= parent::query($STRING);
       
        $this->THEQUIRY->setFetchMode(self::fetchMODE('assoc'));
        
       //$this->THEQUIRY->execute(); 
       $rows = $this->THEQUIRY->fetchAll();
       //$rows =$this->THEQUIRY->fetch(self::fetchMODE('assoc'), 1 ,2);
       $this->THEQUIRY = null;
       unset($this->THEQUIRY);
       return  $rows;
       
    }
   
    public function where($col , $val) 
    {
        $val = parent::quote($val);
        if(self::$Where == '')
            {
                if(strrchr($col,"=") == FALSE)
                {
                    $col .=" = ";
                }

                self::$Where .= " WHERE $col  $val ";    
        }  else {
            if(strrchr($col,"=") == FALSE)
                {
                    $col .=" = ";
                }
                self::$Where .= " AND $col  $val ";   
        }
        return  self::$Where;
        
        
    }
    
  
    
    
    public function or_where($col , $val)
    {
        if(self::$Where == '')
        {
            return $this->where($col, $val);
        }  else 
        {
            
            $val = parent::quote($val);
            
             if(strrchr($col,"=") == FALSE)
             {
                    $col .=" = ";
             }
             self::$Where .=  " OR $col  $val ";
            
        
        }
        
        
        
    }
    
   
    public function where_in(){
        
    }
    
    public function or_where_in(){
        
    }
    
    public function where_not_in(){
        
    }
    
    public function or_where_not_in(){
        
    }
    
    public function where_between(){
        
    }
    
    
    
    
    


    public function like($col , $val) 
    {
        $val = parent::quote($val);
        if(self::$Where == '')
            {
                

                self::$Where .= " WHERE $col LIKE %$val% ";    
        }  else {
           
                self::$Where .= " AND $col LIKE  %$val% ";   
        }
        return  self::$Where;
        
        
    }

     public function or_like($col , $val)
    {
        if(self::$Where == '')
        {
            return $this->like($col, $val);
        }  else 
        {
            
            $val = parent::quote($val);
            
             
             self::$Where .=  " OR $col LIKE %$val% ";
            
        
        } 
        
    }

    public function select(Array $arr=null) 
   {
        
            if($arr != null)
            {
                foreach($arr as $coloms)
                {
                    self::$Select .= "`".$coloms."` ,";
                }
            }
            
            self::$Select = preg_replace("/,\z/i", " " ,self::$Select);
            
            return self::$Select;
            
            
        }
        
        
        
        public function select_max(){
            
        }
        public function select_min(){
            
        }
        
        public function select_avg(){
            
        }
        
        public function select_sum(){
            
        }
        
        
        public function select_count()
        {
            
        }
        
        public function group_by(){
            
        }
        
        public function order_by($filed , $type="ASC"){
            
            self::$order = " ORDER BY  `".$filed."` $type ";
        }


        public function limit(){
            
        }








        public function select_as(Array $arr=null)
    {
             if($arr != null)
            {
                foreach($arr as $coloms=>$as)
                {
                    $as = (string)$as;
                    self::$Select .= "`".$coloms."` AS "."$as"." ,";
                }
            }
            self::$Select = preg_replace("/,\z/i", " " ,self::$Select);
            
            return self::$Select;
        }

        
        
   public function join($joined , $joins , $type = "inner") 
       {
       self::$joins = " $type JOIN $joined ON $joins ";
       }

    public function get( $table,  $limit = null ,  $offset = NULL ) {
        $st=null;
        if(isset($limit))
        {
            $limit = intval($limit);
            if(isset($offset))
            {
                
                $offset = intval($offset);
                $st = "LIMIT ".$limit ." , ". $offset;
                
                //$this->prepaerd->setAttribute(1, 1);
            }  else {
                $st = "LIMIT ".$limit ;
            //$this->prepaerd->setAttribute("LIMIT", $limit );    
            }
            
        }
        $w = self::$Where;
        if(self::$Select == null)
        {
            $sword = " * ";
        }  else {
            $sword = " ".self::$Select." ";
        }
        $this->prepaerd = parent::prepare("SELECT ".$sword." FROM `".$table."` ". self::$joins ."$w ".$st ." ".self::$order);
       // var_dump($this->prepaerd->queryString);
        $this->prepaerd->setFetchMode(self::fetchMODE("obj"));
                self::$Where = null;
                self::$Select = null;
                self::$joins = null;
                $this->THEQUIRY = null;
        $this->prepaerd->closeCursor();
        $this->prepaerd->execute();
         $rows = $this->prepaerd->fetchAll(); 
         $this->prepaerd = null;
         unset($this->prepaerd);
         return $rows;
        
        
        
    }

    
    public function insert($table , $data)
      {
        self::__destruct();
        $this->_placeholder($data);
        
        $sql = $this->_insert($data);
        $statment = "INSERT INTO `".$table."` ";
        $statment .= $sql;
        try {
            
            $this->prepaerd = parent::prepare($statment);
                foreach ($data as $hloder=>$input)
                {
                    //$input = parent::quote($input);
                    $this->prepaerd->bindParam(":$hloder", $input);
                    unset($input);
                    
                }
                
                 $this->prepaerd->execute();
                 unset($this->prepaerd);
                  
            }  catch (Exception $e)
            {
                show_error($e->getMessage()."<br/> ".$e->getFile()." LINE no ".$e->getLine());
            }
           
      }
    
      public function _insert($data)
      {
          $this->_placeholder($data);
          
          $filds = implode(",", self::$placeholders["fildes"]);
          $holde = implode(",", self::$placeholders["holders"]);
          
          $sql = " ( ".$filds."  ) ";
          $sql .= " VALUES ( ".$holde." ) ";
          
          return $sql;
          
          
          
      }


      private function _placeholder($arrdata)
      {
          self::$placeholders = null;
          foreach ($arrdata as $key => $data)
          {
            self::$placeholders["fildes"][] = (string) "`".$key."`" ;
            self::$placeholders["holders"][]  = (string) ":".$key."";
            
          }
          
      }

      private function _preupdates($data)
      {
           self::$placeholders = null;
           foreach ($arrdata as $key => $data)
            {
            self::$placeholders[] = "$key = " . ":$key" ;
            
            }   
      }
      
      public function update($table , $Where , $data){
        self::__destruct();
        $this->_preupdates($data);
        $set = implode(",", self::$placeholders);
        
        $sql = "UPDATE `".$table."` SET ".$set." WHERE $Where ";
        try {
            $stat = parent::prepare($sql);
            
             foreach ($data as $hloder=>$input)
                {
                    $input = parent::quote($input);
                    $stat->bindParam(":$hloder", $input);

                }
                return $stat->execute();
            
            
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }

        
        
        
        
    }
    
    public function delete($table , $Where  , $data , $limit = 1){
        self::__destruct();
        
        $sql = "DELETE FROM `".$table."` WHERE $Where = :$Where "."LIMIT $limit";
        
         try {
            $stat = parent::prepare($sql);
            
             foreach ($data as $hloder=>$input)
                {
                    $input = parent::quote($input);
                    $stat->bindParam(":$hloder", $input);

                }
                return $stat->execute();
            
            
            } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }


    public  function clean($satatment)
    {
        
        $this->SQL = $satatment;
      
        return $this;
    }
    
  
    
    
    
    public function __destruct() {
        self::$Select = null;
        self::$Where = null;
        self::$joins = null;
        $this->prepaerd = null;
        $this->THEQUIRY = null;
        self::$placeholders = null;
    }
    
}

