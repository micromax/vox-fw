<?php

include_once 'Db'.EXT;
/**
 * Description of db_factory
 *
 * @author memo
 */
class db_factory{
    
    public static $dbh;
    public function __construct() {
        self::connect();
    }
    public static function connect()
     {
        
        if(file_exists(APPPATH.'config/database'.EXT))
        {
         require (APPPATH.'config/database'.EXT);   
         //print_r($db["default"]);
         extract($db["default"]);
         
        }  else {
            trigger_error("Missing configration File database ", 'E_USER_NOTICE');
            die();
        }
        
        
       if(empty($dbdriver))
       {
            trigger_error("Please Set dbdriver ", 'E_USER_NOTICE');
            exit();
       }
       
   try {
        switch (strtoupper($dbdriver))
        {
            case 'DBLIB':
                self::$dbh = new DB("dblib:host=$hostname:$dbh_port;dbname=$database;charset=$char_set","$username","$password");
                
            break;
            case 'FIREBIRD': 
                self::$dbh = new DB("firebird:dbname=$database","$username", "$password");

            break;
            case 'IBM': 
                self::$dbh = new DB("ibm:DRIVER={IBM DB2 ODBC DRIVER};DATABASE=$database;"."HOSTNAME=$hostname;PORT=$dbh_port;PROTOCOL=TCPIP;", "$username", "$password");
            break;
            case 'INFORMIX':
                self::$dbh = new DB("informix:host=$hostname; service=9800; 
                        database=$database; server=ids_server; protocol=onsoctcp;
                        EnableScrollableCursors=1", "$username", "$password");

            break;
            case 'MYSQL':
                self::$dbh = new DB("mysql:host=$hostname;dbname=$database","$username","$password",array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES $char_set" , PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET $char_set" ));
                
            break;
            case 'OCI':
                self::$dbh = new DB("oci:dbname=$hostname/$database;charset=$char_set", "$username", "$password");
            break;
            case 'ODBC':
                self::$dbh = new DB("odbc:Driver={SQL Native Client};Server=$hostname;Database=$database;Uid=$username;Pwd=$password;");
            break;
            case 'PGSQL':
               self::$dbh = new DB("pgsql:dbname=$database;user=$username;password=$password;host=$hostname");
            break;
            case 'SQLITE':
               self::$dbh = new DB("sqlite:$database");
            break;
            case '4D':
               self::$dbh = new DB("4D:host=$hostname;charset=$char_set","$username","$password");
            break;
        default :
                trigger_error("This DB Driver does not support!");
            break;
            
        
            
            
        }
       self::$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        
        return self::$dbh;  
       }  catch (PDOException $e)
       {
           echo $e->getMessage();
       }
     }
    
     public function __destruct() {
         self::$dbh = NULL;
        unset(self::$dbh);
     }
    
}

