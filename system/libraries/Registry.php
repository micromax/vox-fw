<?php
/*****************************************************
 * @package        VOX _ framework 
 * @filename        system/libraries/Registry.php
 * @author         mohamed alaa 
 * @copyright       (c) 2012.
 * @since            Version 1.0
 * @filesource
 * @license         GPL 2.0
 * 
 ********************************************************/

// interface 

abstract class Airbox_Registry
{
    abstract protected function get($key);
    // get sotred object 
    abstract protected function set($key , $val);
    // set objects
    
}

class Object_Registry extends Airbox_Registry
{
    
    /**
    * Registry array of objects
    * @access private
    */

    private static $objects = array();
    
    /**
    * The instance of the registry
    * @access private
    */
    
    private static $instance;
    
    //prevent directly access.
    private function __construct(){}
    //prevent clone.
    public function __clone(){}


    /**
    * singleton method used to access the object
    * @access public
    */

   public static function singleton()
   {
       if(!isset(self::$instance))
       {
           self::$instance = new self();
       }
       
       return self::$instance;
   }
   
   protected function get($key) 
   {
       if(isset($this->objects[$key]))
       {
           return $this->objects[$key];
       }
       else
       {
           return NULL;
       }
   }

   protected function set($key, $val) {
       $this->objects[$key] = $val;
   }
    
   //static get request handle
   
   static function getObject($key)
   {
       return self::singleton()->get($key);
   }
   static function storeObject($key , $instance)
   {
       return self::singleton()->set($key,$instance);
   }
   
   
   
}

function Register($class , $arg_array = NULL)
{
    
    $Obj = Object_Registry::singleton();
    
    $Class = strtolower($class); //lowercase classname.
    
    if($Obj->getObject($Class) !== NULL)
    {
        return $Obj->getObject($Class);
    }
    
    if(file_exists(BASEPATH.'libraries'.DIRECTORY_SEPARATOR.ucfirst($Class).EXT))
    {
        require(BASEPATH.'libraries'.DIRECTORY_SEPARATOR.ucfirst($Class).EXT);
    }elseif (file_exists(APPPATH.'libraries'.DIRECTORY_SEPARATOR.ucfirst($Class).EXT)) 
    {
        require(APPPATH.'libraries'.DIRECTORY_SEPARATOR.ucfirst($Class).EXT);
    }
 else {
        //than load it if it's core class
        if(file_exists(BASEPATH.'core'.DIRECTORY_SEPARATOR.ucfirst($Class).EXT))
            {
                require(BASEPATH.'core'.DIRECTORY_SEPARATOR.ucfirst($Class).EXT);
            }
     else {
       trigger_error("File not exists ".$Class);
        die();
        }
      
    }
    
    $classname = ucfirst($class);
    if(is_array($arg_array))
    {
        
        $Obj->storeObject($Class, new $classname($arg_array));
    }elseif ($arg_array != null) {
         $Obj->storeObject($Class, new $classname($arg_array));
        
    }else
    {
    $Obj->storeObject($Class, new $classname());
    }
    $Object = $Obj->getObject($Class);
    if(is_object($Object))
        return $Object;
 
    
}


